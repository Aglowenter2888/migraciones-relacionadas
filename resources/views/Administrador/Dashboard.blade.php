@extends('layouts.admin')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')
   
<div class="panel-header" style="background: #69bb85;">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                    <h5 class="text-white op-7 mb-2">Sistema Repositorio de Producción Académica</h5>
                </div>
              
            </div>
        </div>
    </div>
    <div class="page-inner mt--5">
    
    <!-- Contenido del Dashboard -->
        <div class="row">
            <!-- Cuadros de menu -->
            <div class="col-md-6 col-xl-6">
                <div class="card p-3 bg-info text-white ">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="fa fa-user fa-2x"></i>
                        </div>
                        <br>
                        <h1 class="font-bold">{{ auth()->user()->name}}</h1>
                        <h2 class="">Nivel: {{ auth()->user()->programa}}</h2>
                        <h2 class="">Grado de Estudios: {{ auth()->user()->grado}}</h2>
                       </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-6">
                <div class="card p-3 text-white" style="background: #69bb85;">
                    <div class="card-body p-3 text-center text-white ">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="fa fa-university fa-2x"></i>
                        </div>
                        <br>
                        <h1 class="font-bold">{{ auth()->user()->email}}</h1>
                        <h2 class="">Curp: {{ auth()->user()->curp}}</h2>
                        <h2 class="">RFC: {{ auth()->user()->rfc}}</h2>
                    </div>
                </div>
            </div>
          
        </div>
 <!-- Tabla Documentos -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Documentos Almacenados: {{$documents->count()}}</div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-right p-3">
                        <table  class="table table-striped table-responsive" >
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th>Autor</th>
                                <th>Ver</th>
                                </tr>
                                @foreach ($documents as $key=>$data)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$data->titulo}}</td>
                                    <td>{{$data->autor}}</td>
                                
                                    <td><a class="btn btn-success" href="/files/{{$data->id}}">ver</a></td>
                                
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
 <!-- Tabla Perfiles Activos -->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Usuarios Activos: {{$users->count() }}</div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-right p-3">
                        <table  class="table table-striped table-responsive" >
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Role</th>
                                <th>Division</th>
                                </tr>
                                @foreach ($users as $key=>$data)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->cat_profesor}}</td>
                                    <td>{{$data->division}}</td>                 
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

