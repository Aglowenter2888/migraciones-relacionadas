<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});
//Administrador
Route::get('/Panel-Administrador', function () {
    return view('Administrador.Dashboard');
});

Route::get('/AdminInforme', function () {
    return view('Administrador.Informe');
});

Route::get('/AdminUsuarios','AdminController@usuarios');

Route::get('/AdminDocumentos','AdminController@documentos');


Route::get('/home', function () {
    return view('home');
});

Route::get('/capturaExp', function () {
    return view('expedientes.create');
});

Route::get('/header', function () {
    return view('layouts.header');
});

Route::get('/perfil', function () {
    return view('users.perfil');
});
Route::get('/tutoria', function () {
    return view('pdf.docencia.tutoria');
});

Route::get('/GestionAcademica', function () {
    return view('pdf.academica.gestionAcademica');
});

Route::get('/capacitacion', function () {
    return view('users.capacitacion');
});


// Route::group(['middleware' => ['role:PTC']], function () {
//     Route::get('/Articulo', function () {
//     return view('pdf.academica.articulo');
// });
// });

Route::get('/Articulo-Revista', function () {
    return view('pdf.academica.Articulo_revista');
});
Route::get('/Articulo-Arbitrado', function () {
    return view('pdf.academica.Articulo_Arbitrado');
});

Route::get('/asesorias', function () {
    return view('pdf.academica.Asesorias');
});

Route::get('/Libro', function () {
    return view('pdf.academica.libro');
});

Route::get('/Informe_tec', function () {
    return view('pdf.academica.informe_tec');
});

Route::get('/Proyecto', function () {
    return view('pdf.academica.proyecto');
});

Route::get('/material', function () {
    return view('pdf.academica.Material');
});

Route::get('/curso', function () {
    return view('pdf.docencia.curso');
});

Route::get('/tutoriaGrupal', function () {
    return view('pdf.docencia.tutoria_grupal');
});

Route::get('/direccionIndividualizada', function () {
    return view('pdf.docencia.direccion_individualizada');
});

Route::get('/estadiaEmpresa', function () {
    return view('pdf.docencia.estadia');
});

Route::get('/cuerpoAcademico', function () {
    return view('pdf.docencia.cuerpo_academico');
});

Route::get('/informes', function () {
    return view('supervisor.informe');
});

Route::get('/consultaInfo', function () {
    return view('supervisor.consulta');
});

Route::get('/supervisorDash', function () {
    return view('supervisor.dashboard');
});

//PDF capturaExp
Route::get('/capturaExp', 'PdfController@pdfForm');
Route::post('pdf_download', 'PdfController@pdfDownload');

//PDF Articulo

Route::group(['middleware' => ['role:PTC']], function () {
    Route::get('/articulo', 'PdfController@pdfFormArticulo');
});
Route::post('pdf_download_Articulo', 'PdfController@pdfDownload_Articulo');

//PDF Material
Route::get('/Material', 'PdfController@pdfFormMaterial');
Route::post('pdf_download_Material', 'PdfController@pdfDownload_Material');

//PDF Asesorias
Route::get('/Asesorias', 'PdfController@pdfFormAsesorias');
Route::post('pdf_download_Asesorias', 'PdfController@pdfDownload_Asesorias');

//PDF Proyecto
Route::get('/proyecto', 'PdfController@pdfFormProyecto');
Route::post('pdf_download_Proyecto', 'PdfController@pdfDownload_Proyectos');

//PDF Revista
Route::get('/Articulo_revista', 'PdfController@pdfFormRevista');
Route::post('pdf_download_Revista', 'PdfController@pdfDownload_Revista');

//PDF Arbitrado
Route::get('/Articulo_Arbitrado', 'PdfController@pdfFormArbitrado');
Route::post('pdf_download_Arbitrado', 'PdfController@pdfDownload_Arbitrado');

//PDF Libro
Route::get('/libro', 'PdfController@pdfFormLibro');
Route::post('pdf_download_Libro', 'PdfController@pdfDownload_Libro');

//PDF Informe
Route::get('/informe_tec', 'PdfController@pdfFormInforme');
Route::post('pdf_download_Informe', 'PdfController@pdfDownload_Informe');

Route::post('pdf_download_Curso', 'PdfController@pdfDownload_Curso');

Route::post('pdf_download_Tutoria', 'PdfController@pdfDownload_Tutoria');

Route::post('pdf_download_Grupal', 'PdfController@pdfDownload_Tutoria_Grupal');

Route::post('pdf_download_Direccion_Individualizada', 'PdfController@pdfDownload_Direccion_Individualizada');

Route::post('pdf_download_Estadia_Empresa', 'PdfController@pdfDownload_Estadia');

Route::post('pdf_download_Cuerpo_Academico', 'PdfController@pdfDownload_CuerpoAcademico');

//Home
Route::get('/home','HomeController@index');

//Documentos
Route::get('/files/create','DocumentosController@create');
Route::post('/files','DocumentosController@store');

Route::get('/files','DocumentosController@index');
Route::get('/files/{id}','DocumentosController@show');
Route::get('file/download/{file}','DocumentosController@download');

//Busqueda
Route::get('/search','DocumentosController@search');
Route::get('/Adminsearch','AdminController@Adminsearch');
Route::get('/fechas','DocumentosController@fechas');
Route::get('/fechaActual','DocumentosController@fechaActual');
Route::get('/division','AdminController@PorDivision');
Route::get('/nombre','AdminController@PorNombre');
Route::get('/fechasAdmin','AdminController@fechas');
Route::get('/fechaActualAdmin','AdminController@fechaActual');

//Resources
Route::resource('archivos','DocumentosController');
Route::resource('usuario','UserController');
Route::resource('Administrador','AdminController');

Auth::routes();
